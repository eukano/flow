package golang

import (
	"fmt"
	"strings"

	"gitlab.com/firelizzard/flow/intermediate"
)

type writer func(fmt string, v ...interface{})

func Compile(pkg *intermediate.Package) (string, error) {
	b := new(strings.Builder)
	w := func(f string, v ...interface{}) {
		b.WriteString(fmt.Sprintf(f, v...))
	}

	w("package %s\n\n", pkg.Name)

	if len(pkg.Root.Calls) != 0 {
		return "", fmt.Errorf("unexpected call in root scope") // TODO: support maybe
	}

	for _, decl := range pkg.Root.Declarations {
		if len(decl.Declarations) != 0 {
			return "", fmt.Errorf("unexpected nested declaration") // TODO: support maybe
		}

		w("func %s(", decl.Name)
		for i, net := range decl.In {
			if i > 0 {
				w(", ")
			}
			w("%s %s", net.Name, net.Type)
		}
		w(")")
		if len(decl.Out) != 0 {
			w(" (")
			for i, net := range decl.Out {
				if i > 0 {
					w(", ")
				}
				w(net.Type)
			}
			w(")")
		}
		w(" {\n")

		for _, call := range decl.Calls {
			w("\t")
			err := writeCall(w, call.Name, call.In, call.Out)
			if err != nil {
				return "", err
			}
			w("\n")
		}

		if len(decl.Out) != 0 {
			w("\treturn ")
			for i, net := range decl.Out {
				if i > 0 {
					w(", ")
				}
				w(net.Name)
			}
			w("\n")
		}
		w("}\n\n")
	}

	return b.String(), nil
}

func writeCall(w writer, name string, in, out []*intermediate.Net) error {
	switch name {
	case "+", "-", "*", "/", "%":
		if len(in) < 2 {
			return fmt.Errorf("not enough arguments for %q", name)
		}
		if len(out) != 1 {
			return fmt.Errorf("expected one result for %q but got %d", name, len(out))
		}
		w("%s := ", out[0].Name)
		for i, net := range in {
			if i > 0 {
				w(" %s ", name)
			}
			w(net.Name)
		}

	case "const":
		if len(in) != 1 || len(out) != 1 {
			return fmt.Errorf("expected one argument and one result for %s but got %d and %d", name, len(in), len(out))
		}
		w("const %s = %s", out[0].Name, in[0].Name)

	default:
		if len(out) > 0 {
			for i, net := range out {
				if i > 0 {
					w(", ")
				}
				w(net.Name)
			}
			w(" := ")
		}
		w("%s(", name)
		for i, net := range in {
			if i > 0 {
				w(", ")
			}
			w(net.Name)
		}
		w(")")
	}

	return nil
}
