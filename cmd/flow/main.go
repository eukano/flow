package main

import (
	"fmt"
	"os"

	"gitlab.com/firelizzard/flow/backend/golang"
	"gitlab.com/firelizzard/flow/frontend"
	"gitlab.com/firelizzard/flow/frontend/simple"
	"gitlab.com/firelizzard/flow/intermediate"
)

func main() {
	var units []*frontend.Unit
	var err error

	switch len(os.Args) {
	case 1:
		units, err = simple.Parse("stdin", os.Stdin)

	case 2:
		units, err = simple.ParseFile(os.Args[1])

	default:
		fmt.Fprintf(os.Stderr, "Usage: flow [FILE]\n")
		os.Exit(1)
	}

	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to parse: %v\n", err)
		os.Exit(1)
	}

	il, err := intermediate.Parse(units)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to process: %v\n", err)
		os.Exit(1)
	}

	s, err := golang.Compile(il)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to compile: %v\n", err)
		os.Exit(1)
	}

	_, err = os.Stdout.WriteString(s)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to write: %v\n", err)
		os.Exit(1)
	}
}
