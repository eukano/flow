package intermediate

type Package struct {
	Name string
	Root *Scope
}

type Scope struct {
	Calls        []*Call
	Declarations []*Callable
}

type Call struct {
	Name       string
	Parameters []string
	In, Out    []*Net
}

type Net struct {
	Name string
}

type TypedNet struct {
	Net
	Type string
}

type Callable struct {
	Scope
	Name    string
	In, Out []*TypedNet
}
