package intermediate

import (
	"fmt"

	"gitlab.com/firelizzard/flow/frontend"
)

func Parse(units []*frontend.Unit) (*Package, error) {
	pkg := &Package{
		Root: &Scope{},
	}

	if len(units) == 0 || units[0].Type != "scope" {
		return nil, fmt.Errorf("missing root scope")
	}

	root := units[0]
	if len(root.Calls) != 0 {
		return nil, fmt.Errorf("calls in root scope are not supported")
	}

	for _, dir := range root.Directives {
		switch dir.Name {
		case "package":
			if pkg.Name != "" {
				return nil, fmt.Errorf("unexpected #package")
			}

			if len(dir.Parameters) != 1 {
				return nil, fmt.Errorf("expected #package to have exactly one value")
			}
			pkg.Name = dir.Parameters[0]

		default:
			return nil, fmt.Errorf("unsupported directive #%s", dir.Name)
		}
	}

	for _, unit := range units[1:] {
		decl := new(Callable)
		pkg.Root.Declarations = append(pkg.Root.Declarations, decl)

		switch unit.Type {
		case "main":
			if unit.Name != "" {
				return nil, fmt.Errorf("@main cannot be named")
			}
			decl.Name = "main"

		case "callable":
			decl.Name = unit.Name

		default:
			return nil, fmt.Errorf("unsupported unit @%s", unit.Name)
		}

		for _, dir := range unit.Directives {
			switch dir.Name {
			case "in":
				if len(dir.Parameters) < 2 {
					return nil, fmt.Errorf("#in must have at least one net name followed by a type")
				}
				typ := dir.Parameters[len(dir.Parameters)-1]
				for _, p := range dir.Parameters[:len(dir.Parameters)-1] {
					decl.In = append(decl.In, &TypedNet{Net{Name: p}, typ})
				}

			case "out":
				if len(dir.Parameters) < 2 {
					return nil, fmt.Errorf("#out must have at least one net name followed by a type")
				}
				typ := dir.Parameters[len(dir.Parameters)-1]
				for _, p := range dir.Parameters[:len(dir.Parameters)-1] {
					decl.Out = append(decl.Out, &TypedNet{Net{Name: p}, typ})
				}

			default:
				return nil, fmt.Errorf("unsupported directive #%s", dir.Name)
			}
		}

		for _, c := range unit.Calls {
			call := &Call{
				Name:       c.Name,
				Parameters: c.Parameters,
			}
			decl.Calls = append(decl.Calls, call)

			out := false
			for _, v := range c.Nets {
				switch v {
				case ">", "->", "~>", "=>":
					if out {
						return nil, fmt.Errorf("unexpected %q", v)
					}
					out = true

				default:
					if out {
						call.Out = append(call.Out, &Net{v})
					} else {
						call.In = append(call.In, &Net{v})
					}
				}
			}
		}
	}

	return pkg, nil
}
