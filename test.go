package main

func main() {
	const f = 1
	const g = 2
	h := add(f, g)
	println(h)
}

func add(a int, b int) (int) {
	x := a + b
	return x
}

