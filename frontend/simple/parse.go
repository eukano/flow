package simple

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
	"unicode"

	"gitlab.com/firelizzard/flow/frontend"
)

func runelen(s string) int {
	return len([]rune(s))
}

func runeat(s string, i int) rune {
	return []rune(s)[i]
}

func ParseFile(path string) ([]*frontend.Unit, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return Parse(path, f)
}

func Parse(name string, r io.Reader) (_ []*frontend.Unit, err error) {
	rd := bufio.NewReader(r)

	var root = &frontend.Unit{Name: name, Type: "scope"}
	var units = []*frontend.Unit{root}
	var scope = root
	var line int

	defer func() {
		if err != nil {
			err = fmt.Errorf("%v [%s:%d]", err, name, line)
		}
	}()

	for {
		line++
		s, err := rd.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		i := strings.Index(s, "//")
		if i >= 0 {
			s = s[:i]
		}

		f := []string{}
		for _, v := range strings.Fields(s) {
			for {
				i := strings.IndexAny(v, "[]{}")
				if i < 0 {
					break
				}

				if i > 0 {
					f = append(f, v[:i])
				}
				f = append(f, string(runeat(v, i)))
				v = v[i+1:]
			}

			if v != "" {
				f = append(f, v)
			}
		}

		if len(f) == 0 {
			continue
		}

		switch runeat(f[0], 0) {
		case '@':
			if runelen(f[0]) == 1 || unicode.IsSpace(runeat(f[0], 1)) {
				return nil, fmt.Errorf("invalid unit '@'")
			}

			i = len(f) - 1
			if f[i] != "{" {
				return nil, fmt.Errorf("expected '{' but got EOL")
			}
			f = f[:i]

			if scope != root {
				return nil, fmt.Errorf("units cannot be nested") // TODO: unit nesting
			}

			scope = &frontend.Unit{Type: f[0][1:]}
			if len(f) >= 2 {
				scope.Name = f[1]
			}
			if len(f) >= 3 {
				return nil, fmt.Errorf("too many values in unit declaration")
			}

			units = append(units, scope)

		case '}':
			if runelen(f[0]) != 1 {
				return nil, fmt.Errorf("unexpected characters after '}'")
			}
			scope = root

		case '#':
			if runelen(f[0]) == 1 || unicode.IsSpace(runeat(f[0], 1)) {
				return nil, fmt.Errorf("invalid directive '#'")
			}

			dir := &frontend.Directive{
				Name:       f[0][1:],
				Parameters: f[1:],
			}
			scope.Directives = append(scope.Directives, dir)

		default:
			call := &frontend.Call{Name: f[0]}
			scope.Calls = append(scope.Calls, call)

			params := false
			for _, v := range f[1:] {
				switch v {
				case "{", "}":
					return nil, fmt.Errorf("unexpected %q", v)

				case "[":
					if params {
						return nil, fmt.Errorf("unexpected '['")
					}
					if call.Parameters != nil {
						return nil, fmt.Errorf("call cannot specify parameters twice")
					}
					params = true
					call.Parameters = []string{}

				case "]":
					if !params {
						return nil, fmt.Errorf("unexpected ']'")
					}
					params = false

				default:
					if params {
						call.Parameters = append(call.Parameters, v)
					} else {
						call.Nets = append(call.Nets, v)
					}
				}
			}
		}
	}

	return units, nil
}
