package frontend

type Directive struct {
	Name       string
	Parameters []string
}

type Call struct {
	Name       string
	Parameters []string
	Nets       []string
}

type Unit struct {
	Name, Type string
	Directives []*Directive
	Calls      []*Call
}
